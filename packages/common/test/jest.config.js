const PWD = process.cwd();

module.exports = {
  bail: true,
  coverageDirectory: '<rootDir>/public/coverage',
  moduleNameMapper: {
    // Generic return values for otherwise ignored
    // imports that would throw a syntax error:
    '\\.css$': '<rootDir>/test/double/dummy/style.js',
    '\\.svg$': '<rootDir>/test/double/dummy/vector.js',
  },
  modulePaths: ['<rootDir>/vendor/node_modules'],
  rootDir: PWD,
  setupFilesAfterEnv: ['<rootDir>/test/jest.setup.js'],
  testMatch: ['<rootDir>/src/**/*.test.js'],
};
