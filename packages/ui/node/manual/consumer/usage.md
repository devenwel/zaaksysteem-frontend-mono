# Usage

## Imports

### CSS

    import '@mintlab/ui/distribution/ui.css';

### React Components

    import { Resource } from '@mintlab/ui';

### Caveats

This package expects to be built into the public path 
`/vendor/` by the consuming application.

### See also

[API documentation](../identifiers.html)
