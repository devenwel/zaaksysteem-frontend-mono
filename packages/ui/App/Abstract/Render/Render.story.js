import { React, UseTheKnobs, stories, boolean, text } from '../../story';
import Render from '.';

stories(module, __dirname, {
  Default() {
    return (
      <div>
        <UseTheKnobs />
        <Render condition={boolean('Active', false)}>
          <h1>{text('Greeting', 'Hello, world!')}</h1>
        </Render>
      </div>
    );
  },
});
