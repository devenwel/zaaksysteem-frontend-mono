import { session } from '@zaaksysteem/common/src/store/session/session.reducer';
import { sessionMiddleware } from '@zaaksysteem/common/src/store/session/session.middleware';
import { fetchSession } from '@zaaksysteem/common/src/store/session/session.actions';

export const getSessionModule = () => ({
  id: 'session',
  reducerMap: {
    session,
  },
  initialActions: [fetchSession()],
  middlewares: [sessionMiddleware],
});

export default getSessionModule;
