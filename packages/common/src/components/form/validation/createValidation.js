import * as yup from 'yup';

/**
 * Creates a Yup schema based on the provided formDefinition,
 * validationMap and translation function. If a field is hidden
 * or disabled, a given rule will not be added.
 *
 * This schema is then validated against the provided form values.
 *
 * Returns a promise that resolves with the yupErrors object.
 *
 * @param {Array} formDefinition
 * @param {Object} values
 * @param {Object} validationMap
 * @param {Function} t
 * @return {Promise}
 **/
export const createValidation = ({
  formDefinition,
  values,
  validationMap,
  t,
}) => {
  yup.setLocale({
    mixed: t('validations:mixed', { returnObjects: true }),
  });

  const rule = field => {
    if (field.hidden || field.disabled) {
      return;
    }

    if (validationMap[field.type]) {
      return validationMap[field.type]({ t, field });
    }

    return field.required ? yup.string().required() : yup.string();
  };

  const schema = formDefinition.reduce((accumulator, field) => {
    const resolvedRule = rule(field);

    if (resolvedRule) {
      accumulator[field.name] = resolvedRule;
    }

    return accumulator;
  }, {});

  return yup
    .object()
    .shape(schema)
    .validate(values, { abortEarly: false })
    .then(() => Promise.resolve({}))
    .catch(yupErrors => Promise.resolve(yupErrors));
};

export default createValidation;
