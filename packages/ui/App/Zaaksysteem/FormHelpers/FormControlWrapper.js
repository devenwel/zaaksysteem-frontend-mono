import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { addScopeAttribute } from '../../library/addScope';
import { formControlWrapperStylesheet } from './FormControlWrapper.style';
import Tooltip from '../../Material/Tooltip/Tooltip';
import Render from '../../Abstract/Render/Render';
import InfoIcon from './library/InfoIcon';
import ErrorLabel from './library/ErrorLabel';
import { Subtitle1, Caption } from '../../Material/Typography';

/**
 * Wraps children (usually an input component) with:
 *
 * - A title label
 * - A help tooltip (if provided)
 * - A hint label (if provided)
 * - An error label (if provided)
 *
 * Displays this in either a wide (3 column, default) or
 * compact (stacked) layout.
 *
 * @param {Object} props
 * @param {Object} props.classes
 * @param {ReactElement} props.children
 * @param {string} [props.error]
 * @param {string} [props.help]
 * @param {string} [props.hint='']
 * @param {string} [props.label='']
 * @param {boolean} [props.required=false]
 * @param {boolean} [props.compact=false]
 * @param {string} [props.scope]
 * @param {boolean} [props.touched=false]
 * @return {ReactElement}
 */
export const FormControlWrapper = ({
  classes,
  children,
  error,
  help,
  hint = '',
  label = '',
  required = false,
  compact = false,
  touched = false,
  scope,
}) => {
  const elHelp = (
    <Render condition={help}>
      <div className={classes.help}>
        <Tooltip title={help} type="info">
          <InfoIcon />
        </Tooltip>
      </div>
    </Render>
  );

  const elLabel = (
    <div className={classes.label}>
      <Subtitle1>
        {label}
        <Render condition={required}>
          <sup>﹡</sup>
        </Render>
      </Subtitle1>
    </div>
  );

  const elHint = (
    <Render condition={hint}>
      <div className={classes.hint}>
        <Caption
          classes={{
            root: classes.hint,
          }}
        >
          {hint}
        </Caption>
      </div>
    </Render>
  );

  const elError = (
    <Render condition={error && touched}>
      <div className={classes.error}>
        <ErrorLabel label={error} />
      </div>
    </Render>
  );

  const elControl = <div className={classes.control}>{children}</div>;

  const wrap = content => (
    <div
      className={classes.wrapper}
      {...addScopeAttribute(scope, 'formControlWrapper')}
    >
      {content}
    </div>
  );

  if (compact) {
    return wrap(
      <Fragment>
        {elLabel}
        {elHelp}
        {elHint}
        {elControl}
        {elError}
      </Fragment>
    );
  }

  return wrap(
    <Fragment>
      <div className={classes.colLabels}>
        {elLabel} {elHint}
      </div>
      <div className={classes.colContent}>
        {elControl}
        {elError}
      </div>
      <div className={classes.colHelp}>{elHelp}</div>
    </Fragment>
  );
};

export default withStyles(formControlWrapperStylesheet)(FormControlWrapper);
