# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.4.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.4.0...@zaaksysteem/main@0.4.1) (2019-07-30)

**Note:** Version bump only for package @zaaksysteem/main





# [0.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.3.0...@zaaksysteem/main@0.4.0) (2019-07-29)


### Features

* **Dates:** MINTY-1120 Add localized date formatting lib `fecha` ([84b6693](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/84b6693))





# [0.3.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.2.0...@zaaksysteem/main@0.3.0) (2019-07-29)


### Features

* **i18n:** MINTY-1120 Add i18next to setup and define APP_CONTEXT_ROOT variable on build time. ([a3e6a4c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a3e6a4c))





# [0.2.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.1.0...@zaaksysteem/main@0.2.0) (2019-07-25)


### Features

* **main:** MINTY-1237 Add session state to @zaaksysteem/common and implement in app ([575898f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/575898f))





# 0.1.0 (2019-07-25)


### Features

* **main:** MINTY-1232 Add `connected-react-router` package to link router to redux store ([a673309](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a673309))





## [0.1.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/compare/@zaaksysteem/case-management@0.1.0...@zaaksysteem/case-management@0.1.1) (2019-07-23)

**Note:** Version bump only for package @zaaksysteem/case-management





# 0.1.0 (2019-07-18)


### Features

* **CI:** MINTY-1120 Add gitlab CI file ([f4e971e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/f4e971e))
* **Publishing:** MINTY-1120 Implement lerna publish command voor npm packages ([35934cc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/35934cc))
* **Setup:** MINTY-1120 Add eslint and prettier ([0e6b94c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/0e6b94c))
* **Setup:** MINTY-1120 Add storybook ([1dc9406](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/1dc9406))
* **Setup:** MINTY-1120 Centralize create react app overrides in common package ([3cdc9d7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/3cdc9d7))
* **Setup:** MINTY-1120 Setup monorepo for all apps and packges using lerna and yarn workspaces ([6bd626e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/6bd626e))
