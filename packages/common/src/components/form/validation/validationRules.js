import * as yup from 'yup';

/**
 * Validation rules for basic component types.
 * Note that rules are responsible for implementing the
 * `required` check, as the order of checks can matter.
 */

export const emailRule = ({ t, field }) => {
  const base = yup.string();
  const { required } = field;
  const message = t('validations:email.invalidEmailAddress');

  return required ? base.required().email(message) : base.email(message);
};

export const selectRule = ({ field }) =>
  field.required ? yup.mixed().required() : yup.mixed();

export const textRule = ({ field }) =>
  field.required ? yup.string().required() : yup.string();
