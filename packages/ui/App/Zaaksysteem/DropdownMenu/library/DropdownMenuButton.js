import React, { Fragment } from 'react';
import MuiButton from '@material-ui/core/Button';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import Icon from '../../../Material/Icon';
import { dropdownMenuButtonStylesheet } from './DropdownMenuButton.style';
import { withStyles } from '@material-ui/core/styles';
import { addScopeAttribute } from '../../../library/addScope';

/**
 * @param {Action} props
 * @param {Function} props.action
 * @param {string} props.icon
 * @param {string} props.label
 * @param {Object} props.classes
 * @param {string} props.scope
 * @return {ReactElement}
 */
export const DropdownMenuButton = ({ action, icon, label, classes, scope }) => {
  const labelComponent = icon ? (
    <Fragment>
      <Icon size="small">{icon}</Icon>
      {label}
    </Fragment>
  ) : (
    label
  );

  return (
    <MuiButton
      onClick={action}
      classes={cloneWithout(classes, 'wrapper')}
      fullWidth={true}
      {...addScopeAttribute(scope, 'button')}
    >
      {labelComponent}
    </MuiButton>
  );
};

export default withStyles(dropdownMenuButtonStylesheet)(DropdownMenuButton);
