import { createBrowserHistory } from 'history';
import { createStore } from 'redux-dynamic-modules-core';
import { getThunkExtension } from 'redux-dynamic-modules-thunk';
import getRouterModule from './store/route/route.module';
import getSessionModule from './store/session/session.module';

export const history = createBrowserHistory();

export const configureStore = initialState => {
  const store = createStore(
    { initialState, extensions: [getThunkExtension()] },
    getRouterModule(history),
    getSessionModule()
  );

  return store;
};
