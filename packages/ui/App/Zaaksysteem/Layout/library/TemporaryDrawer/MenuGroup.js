import React from 'react';
import classNames from 'classnames';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Icon from '../../../../Material/Icon/Icon';
import { Body2 } from '../../../../Material/Typography';
import { menuGroupStylesheet } from './MenuGroup.style';
import { withStyles } from '@material-ui/core/styles';
import { addScopeAttribute } from '../../../../library/addScope';

/**
 * Parse props for either a `button` or an `a` element.
 * The `action` and `href` props are mutually exclusive.
 * The only current use case is to open a new tab or window
 * without script to prevent problems with popup blockers.
 *
 * @param {Object} props
 * @param {Function} [props.action]
 * @param {string} [props.href]
 * @param {string} [props.target]
 * @return {Object}
 */
export function getActionProps({ action, href, target }) {
  if (href) {
    return {
      component: 'a',
      href,
      target,
    };
  }

  return {
    onClick: action,
  };
}

/**
 * @param {Array} navigation
 * @param {Object} classes
 * @param {number} active
 * @return {ReactElement}
 */
export const MenuGroup = ({ navigation, classes, active, scope }) => (
  <MenuList
    component="nav"
    classes={{
      root: classes.menuList,
    }}
    {...addScopeAttribute(scope, 'menu')}
  >
    {navigation.map(({ action, href, icon, label, target }, index) => (
      <MenuItem
        key={index}
        classes={{
          root: classNames(classes.root, {
            [classes.active]: active === index,
          }),
        }}
        {...getActionProps({
          action,
          href,
          target,
        })}
        {...addScopeAttribute(scope, 'menu', label, 'item')}
      >
        <ListItemIcon
          classes={{
            root: classes.inherit,
          }}
        >
          <Icon
            size="small"
            classes={{
              root: classes.inherit,
            }}
          >
            {icon}
          </Icon>
        </ListItemIcon>
        <ListItemText disableTypography={true}>
          <Body2
            classes={{
              root: classes.inherit,
            }}
          >
            {label}
          </Body2>
        </ListItemText>
      </MenuItem>
    ))}
  </MenuList>
);

export default withStyles(menuGroupStylesheet)(MenuGroup);
