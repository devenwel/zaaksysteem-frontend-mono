import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import fecha from 'fecha';
import resources from './locale';

function initFetcha() {
  fecha.i18n = {
    ...fecha.i18n,
    ...i18n.t('common:dates', { returnObjects: true }),
  };
}

i18n.use(initReactI18next).init(
  {
    resources,
    lng: 'nl',
    keySeparator: '.',
    interpolation: {
      escapeValue: false,
    },
    react: {
      wait: true,
    },
  },
  () => initFetcha()
);

export default i18n;
