# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.8.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.8.2...@mintlab/ui@10.8.3) (2019-07-25)

**Note:** Version bump only for package @mintlab/ui





## 10.8.2 (2019-07-23)

**Note:** Version bump only for package @mintlab/ui
