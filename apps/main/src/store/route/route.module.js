import { connectRouter, routerMiddleware } from 'connected-react-router';

export const getRouterModule = history => ({
  id: 'router',
  reducerMap: {
    router: connectRouter(history),
  },
  middlewares: [routerMiddleware(history)],
});

export default getRouterModule;
