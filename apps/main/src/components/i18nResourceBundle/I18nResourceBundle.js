import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

const I18nResourceBundle = ({ resource, namespace, children }) => {
  const { i18n } = useTranslation();

  Object.entries(resource).forEach(([lng, translations]) =>
    i18n.addResourceBundle(lng, namespace, translations)
  );

  useEffect(() => {
    return () => {
      Object.entries(resource).forEach(([lng, translations]) =>
        i18n.removeResourceBundle(lng, namespace, translations)
      );
    };
  });

  return children;
};

export default I18nResourceBundle;
