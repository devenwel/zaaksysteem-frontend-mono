import React from 'react';
import ButtonBase from '@material-ui/core/ButtonBase';
import { withStyles } from '@material-ui/core/styles';
import Icon from '../../../../Material/Icon/Icon';
import { compactButtonStyleSheet } from './CompactButton.style';
import { Caption } from '../../../../Material/Typography';
import classNames from 'classnames';
import { addScopeAttribute } from '../../../../library/addScope';

export const CompactButton = ({
  action,
  classes,
  icon,
  label,
  active = false,
  scope,
}) => (
  <ButtonBase
    classes={{
      root: classNames(classes.root, { [classes.active]: active }),
    }}
    onClick={action}
    {...addScopeAttribute(scope, 'button')}
  >
    <Icon size="small">{icon}</Icon>

    <Caption
      classes={{
        root: classes.label,
      }}
    >
      {label}
    </Caption>
  </ButtonBase>
);

export default withStyles(compactButtonStyleSheet)(CompactButton);
