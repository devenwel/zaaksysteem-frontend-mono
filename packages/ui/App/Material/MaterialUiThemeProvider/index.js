import { default as MaterialUiThemeProvider } from './MaterialUiThemeProvider';
export * from './MaterialUiThemeProvider';
export default MaterialUiThemeProvider;
