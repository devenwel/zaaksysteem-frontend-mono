const requireDirectory = require('require-directory');
require('./webpack/library/entry');

module.exports = {
  mode: process.env.NODE_ENV,
  devtool: process.env.NODE_ENV === 'production' ? false : 'inline-source-map',
  ...requireDirectory(module, './webpack/config'),
};
