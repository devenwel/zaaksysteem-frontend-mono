import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { fileListStylesheet } from './FileList.style';

const FileList = ({ children, classes }) => (
  <div className={classes.wrapper}>{children}</div>
);

export default withStyles(fileListStylesheet)(FileList);
