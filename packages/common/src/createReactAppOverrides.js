const {
  babelInclude,
  getBabelLoader,
  addWebpackAlias,
  useEslintRc,
} = require('customize-cra');
const rewireReactHotLoader = require('react-app-rewire-hot-loader');

const addHotLoaderAlias = (config, env) => {
  if (env === 'production') {
    return config;
  }

  return addWebpackAlias({
    'react-dom': '@hot-loader/react-dom',
  })(config);
};

const setPublicPath = (config, basePath) => {
  config.output.publicPath = basePath;

  return config;
};

const enableHMR = (config, env, basePath) => {
  if (env === 'production') {
    return config;
  }

  const { entry } = rewireReactHotLoader(config, env);

  config.entry = [
    basePath === '/'
      ? 'webpack-dev-server/client'
      : `webpack-dev-server/client?sockPath=${basePath}/sockjs-node`,
    'react-hot-loader/patch',
    'webpack/hot/dev-server',
  ].concat(entry.filter(item => item.indexOf('webpackHotDevClient') === -1));

  return config;
};

const defineVariables = (config, basePath) => {
  const definePlugin = config.plugins.find(
    plugin => typeof plugin.definitions !== 'undefined'
  );
  definePlugin.definitions['process.env'] = {
    ...definePlugin.definitions['process.env'],
    APP_CONTEXT_ROOT: `'${basePath.slice(0, -1)}'`,
  };

  return config;
};

/* eslint-disable no-param-reassign */
module.exports = function createReactAppOverrides(basePath) {
  return {
    webpack(config, env) {
      const { include } = getBabelLoader(config);

      config = addHotLoaderAlias(config);
      config = babelInclude([include, /packages/])(config);
      config = enableHMR(config, env, basePath);
      config = setPublicPath(config, basePath);
      config = useEslintRc('../../.eslintrc.js')(config);
      config = defineVariables(config, basePath);
      return config;
    },
    devServer: configFunction => (proxy, allowedHost) => {
      // Create the default config by calling configFunction with the proxy/allowedHost parameters
      const config = configFunction(proxy, allowedHost);
      config.watchOptions.ignored = /(?!\/apps\/|\/packages\/).+\/node_modules\//g;
      config.clientLogLevel = 'info';
      config.overlay = true;

      // Return your customised Webpack Development Server config.
      return config;
    },
  };
};
