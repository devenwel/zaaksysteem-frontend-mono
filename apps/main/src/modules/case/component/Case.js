import React from 'react';
import { useTranslation } from 'react-i18next';
import { H1 } from '@mintlab/ui/App/Material/Typography';
import Wysiwyg from '@mintlab/ui/App/Zaaksysteem/Wysiwyg';
import fecha from 'fecha';

const Case = ({ name }) => {
  const { t } = useTranslation('case');

  return (
    <React.Fragment>
      <H1>{t('main.welcome', { name })}</H1>
      <p>{t('main.enterText')}</p>
      <p>{fecha.format(new Date())}</p>
      <Wysiwyg />
    </React.Fragment>
  );
};

export default Case;
