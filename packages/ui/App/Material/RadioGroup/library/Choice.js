import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import { addScopeAttribute } from '../../../library/addScope';

/**
 * Facade for *Material-UI* `FormControlLabel`.
 *
 * @param {Object} props
 * @param {boolean} [props.disabled=false]
 * @param {string} props.label
 * @param {string} props.value
 * @param {number} index
 * @return {ReactElement}
 */
export const Choice = ({ key, disabled = false, label, value, scope }) => (
  <FormControlLabel
    key={key}
    control={<Radio />}
    disabled={disabled}
    label={label}
    value={value}
    {...addScopeAttribute(scope, 'choice')}
  />
);
