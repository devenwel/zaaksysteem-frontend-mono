import React, { Component, Fragment } from 'react';
import Menu from '@material-ui/core/Menu';
import { withStyles } from '@material-ui/core/styles';
import { unique, cloneWithout } from '@mintlab/kitchen-sink/source';
import { DropdownMenuStylesheet } from './DropdownMenu.style';
import { addScopeAttribute } from '../../library/addScope';

export { default as DropdownMenuList } from './library/DropdownMenuList';

/**
 * Dropdown menu component. Additional props will be spread to the Menu component.
 *
 * @see https://material-ui.com/api/menu/
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/DropdownMenu
 * @see /npm-mintlab-ui/documentation/consumer/manual/DropdownMenu.html
 *
 * @reactProps {ReactElement} children
 *    The content to be shown in the popup window.
 * @reactProps {ReactElement} trigger
 *    The component that will serve as the trigger to activate the popup window.
 * @reactProps {Object} classes
 * @reactProps {Object} transformOrigin
 *    @see https://material-ui.com/api/popover/
 */
export class DropdownMenu extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      transformOrigin: {
        vertical: 0,
        horizontal: 'right',
      },
    };
  }

  state = {
    anchorEl: null,
  };

  // Lifecycle methods

  render() {
    const {
      handleClick,
      handleClose,
      props: { children, classes, trigger, transformOrigin, scope, ...rest },
      state: { anchorEl },
    } = this;

    const open = Boolean(anchorEl);
    const id = unique();

    // ZS-FIXME: a11y design mistake
    // - span has click handler without role
    // - setting role would require setting tab index
    // - setting tabindex would presumably be wrong because
    //   the trigger prop is likely to have a tab index itself
    /* eslint jsx-a11y/click-events-have-key-events: warn */
    /* eslint jsx-a11y/no-static-element-interactions: warn */
    return (
      <Fragment>
        <span
          aria-owns={this.getOwner(open, id)}
          aria-haspopup="true"
          onClick={handleClick}
          {...addScopeAttribute(scope, 'dropdown-menu')}
        >
          {trigger}
        </span>
        <Menu
          classes={cloneWithout(classes, 'list')}
          anchorEl={anchorEl}
          transformOrigin={transformOrigin}
          id={id}
          onClick={handleClose}
          open={open}
          MenuListProps={{
            classes: {
              root: classes.list,
            },
          }}
          {...rest}
        >
          {children}
        </Menu>
      </Fragment>
    );
  }

  // Custom methods

  /**
   * @param {boolen} open
   * @param {string} id
   * @return {null|string}
   */
  getOwner(open, id) {
    if (open) {
      return id;
    }

    return null;
  }

  /**
   * @return {boolean}
   */
  isHtmlComponent() {
    const { type } = this.props.trigger;

    return typeof type === 'string';
  }

  /**
   * @return {boolean}
   */
  isInteractive() {
    return this.props.trigger.type === 'button';
  }

  /**
   * @param {Event} event
   * @param {Node} event.currentTarget
   */
  handleClick = ({ currentTarget }) => {
    this.setState({
      anchorEl: currentTarget,
    });
  };

  handleClose = () => {
    this.setState({
      anchorEl: null,
    });
  };
}

export default withStyles(DropdownMenuStylesheet)(DropdownMenu);
