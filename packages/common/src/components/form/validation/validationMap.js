import { EMAIL, SELECT, TEXT } from '../constants/fieldTypes';
import * as validationRules from './validationRules';

export default {
  [EMAIL]: validationRules.emailRule,
  [SELECT]: validationRules.selectRule,
  [TEXT]: validationRules.textRule,
};
