import React, { Component } from 'react';
import { withFormik } from 'formik';
import { get, cloneWithout } from '@mintlab/kitchen-sink/source';
import { FormFields } from './fields';
import createValidation from './validation/createValidation';
import defaultValidationMap from './validation/validationMap';
import { withRules } from './rules';
import i18n from '../../i18n';

const getValuesFromDefinition = formDefinition =>
  formDefinition.reduce(
    (values, field) => ({
      ...values,
      [field.name]: field.value,
    }),
    {}
  );

const setTouchedAndHandleChange = ({
  setFieldTouched,
  handleChange,
}) => event => {
  const {
    target: { name },
  } = event;
  setFieldTouched(name, true);
  handleChange(event);
};

const Form = ({
  Fields,
  errors,
  children,
  touched,
  handleBlur,
  setFieldValue,
  setFieldTouched,
  handleChange,
  values,
  formDefinition,
  isValid,
}) => {
  const fields = formDefinition
    .filter(field => !field.hidden)
    .map(field => {
      const { name } = field;

      return {
        ...field,
        definition: field,
        FieldComponent: Fields[field.type],
        value: values[name],
        checked: values[name],
        error: touched[name] ? errors[name] : undefined,
        touched: touched[name],
        key: `field-${name}`,
        refValue: get(values, `${field.refValue}`, null),
        onChange: setTouchedAndHandleChange({
          setFieldTouched,
          handleChange,
        }),
        onBlur: handleBlur,
        setFieldValue,
      };
    });

  return children({
    fields,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    handleChange,
    values,
    isValid,
    errors,
    touched,
  });
};

// eslint-disable valid-jsdoc
class FormRenderer extends Component {
  static defaultProps = {
    rules: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      formDefinition: props.formDefinition,
    };

    const FormComponent =
      props.rules.length > 0
        ? withRules({
            Component: Form,
            setFormDefinition: this.setFormDefinition,
          })
        : Form;

    this.WrappedForm = withFormik({
      enableReinitialize: false,
      isInitialValid: this.props.isInitialValid || true,
      validate: this.validate,
      mapPropsToValues: () =>
        getValuesFromDefinition(this.state.formDefinition),
    })(FormComponent);
  }

  render() {
    const { rules, children, FieldComponents = {}, ...rest } = this.props;

    const Fields = {
      ...FormFields,
      ...FieldComponents,
    };

    const WrappedForm = this.WrappedForm;

    return (
      <WrappedForm
        Fields={Fields}
        formDefinition={this.state.formDefinition}
        rules={rules}
        {...cloneWithout(rest, 'formDefinition')}
      >
        {children}
      </WrappedForm>
    );
  }

  setFormDefinition = formDefinition => this.setState({ formDefinition });

  validate = values => {
    const { validationMap } = this.props;
    const { formDefinition } = this.state;

    const mergedValidationMap = {
      ...defaultValidationMap,
      ...validationMap,
    };

    const validation = createValidation({
      formDefinition,
      validationMap: mergedValidationMap,
      values,
      t: (...args) => i18n.t(...args),
    });

    return validation.then(yupErrors => {
      if (Object.keys(yupErrors).length === 0) return;

      const errors = yupErrors.inner.reduce((acc, error) => {
        const { message, path } = error;
        if (message && !acc[path]) {
          acc[path] = message;
        }
        return acc;
      }, {});

      if (errors) {
        throw errors;
      }
    });
  };
}

export default FormRenderer;
