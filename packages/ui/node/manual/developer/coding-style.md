# Coding style requirements

- Use **both** a *named* and a `default` export 
  for every component.
  - Always import the named export in tests.
  - If you wrap the component in a `HoC`, 
    only do that in the `default` export.

## Stateless functional component

If you don't need lifecycle methods, always prefer a 
stateless functional component. Using an arrow function
is usually concise and readable.

    import React from 'react';
    import withAmazingThings from 'amazing-things';

    export const MyComponent = ({
      amazing, 
      children, 
    }) => (
      <div>My {children} are {amazing}.</div>
    );
    
    export default withAmazingThings()(MyComponent);
    
Sometimes you need to do something based on the passed props
before you `return`. Using a function declaration is also 
fine in that case.

    import React from 'react';

    export function MyComponent({ children }) {
      // do stuff based on the passed props

      return (
        <div>{children}</div>
      );
    }
    
    export default MyComponent;

## Class component

If you **do** need lifecycle methods,
e.g. for DOM access, use a class component.

    import React, { createRef } from 'react';
    
    export default class MyComponent {
        constructor(props) {
          super(props);
          this.domNode = createRef();
        }
    
        // the component's DOM is ready
        componentDidMount() {
          this.domNode.current.addEventListener('exoticEventType', this);
        }

        // always clean up behind manual operations
        componentWillUnmount() {
          this.domNode.current.removeEventListener('exoticEventType', this);
        }
    
        render() {
          const { children } = this.props;
        
          return (
            <div
              ref={this.domNode}
            >{children}</div>
          );
        }
        
        handleEvent(nativeDomEvent) {
          // do stuff when the `exoticEventType` event fires
        }
    }

