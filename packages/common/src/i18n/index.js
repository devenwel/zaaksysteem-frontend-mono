import i18next from 'i18next';
import resources from './locale';

i18next.init({
  lng: 'nl',
  resources,
  keySeparator: '.',
  ns: ['common', 'validations'],
  defaultNS: 'common',
});

export default i18next;
