export default {
  nl: {
    validations: {
      mixed: {
        required: 'Vul een geldige waarde in.',
      },
      email: {
        invalidEmailAddress:
          'Er is geen geldig e-mailadres ingevuld. Suggestie: naam@example.com',
      },
      array: {
        noFile: 'Geen bestand(en) geüpload. Voeg een bestand toe.',
        invalidFile: 'Ongeldig(e) bestand(en) geüpload. Probeer het opnieuw.',
      },
    },
  },
};
