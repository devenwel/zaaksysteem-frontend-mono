import React from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import { getCaseDataModule } from './redux/caseData.module';
import CaseContainer from './component/CaseContainer';
import locale from './locale';
import I18nResourceBundle from '../../components/i18nResourceBundle/I18nResourceBundle';

export default function CaseModule({ match }) {
  return (
    <DynamicModuleLoader modules={[getCaseDataModule(match.params.id)]}>
      <I18nResourceBundle resource={locale} namespace="case">
        <CaseContainer />
      </I18nResourceBundle>
    </DynamicModuleLoader>
  );
}
