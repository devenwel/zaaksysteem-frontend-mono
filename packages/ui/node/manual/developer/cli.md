# CLI commands

In your host, run

    $ docker-compose exec app sh

for standalone development.
 
## Start the storybook

    $ npm start

## Generate a component

    $ npm run plop

- Provide the *component name* and choose a 
  *component type* when prompted.

## Lint the JS files

    $ npm run lint

## Run the tests

    $ npm test

to run the tests once or
    
    $ npm run tdd

to watch the source code for changes during development.

## Combine the previous tasks for lazy quality insurance

    $ npm run qa

A `pre-commit` hook is a good usage candidate.

## Generate this documentation

    $ npm run docs

## Generate the pipeline artefacts and view them on a local server

    $ npm run rtfm

## Dry run pipeline npm scripts (sans publication)

This is a deduped accumulation of the npm scripts 
used in the ci pipeline:

    $ npm run dry

It's quite a mouthful, so you'd only want to 
do this before proposing a merge request,
either manually or automated 
(e.g. as a git `pre-push` hook).
