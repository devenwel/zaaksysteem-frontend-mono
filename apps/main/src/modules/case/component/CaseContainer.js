import { connect } from 'react-redux';
import Case from './Case';

const mapStateToProps = state => {
  if (!state.caseData) return;

  const {
    session: {
      data: {
        logged_in_user: { display_name: name },
      },
    },
  } = state;

  return { name };
};

const CaseContainer = connect(mapStateToProps)(Case);

export default CaseContainer;
