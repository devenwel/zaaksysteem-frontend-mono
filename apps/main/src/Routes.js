import React, { Suspense } from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import { AJAX_STATE_VALID } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';

const CaseModule = React.lazy(() =>
  import(/* webpackChunkName: "case" */ './modules/case')
);

const Routes = ({ ready, prefix }) => {
  if (!ready) {
    return <Loader />;
  }

  return (
    <Suspense fallback={<Loader />}>
      <Switch>
        <Route
          path={`${prefix}/case/:id/communication`}
          component={CaseModule}
        />
        <Route path="*" component={CaseModule} />
      </Switch>
    </Suspense>
  );
};

const mapStateToProps = ({ session: { state } }) => ({
  ready: state === AJAX_STATE_VALID,
});

export default connect(mapStateToProps)(Routes);
