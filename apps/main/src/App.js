import React from 'react';
import { hot } from 'react-hot-loader/root';
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux';
import MaterialUiThemeProvider from '@mintlab/ui/App/Material/MaterialUiThemeProvider/MaterialUiThemeProvider';
import { configureStore, history } from './configureStore';
import Routes from './Routes';

const initialState = {};
const store = configureStore(initialState);

function App() {
  return (
    <Provider store={store}>
      <MaterialUiThemeProvider>
        <ConnectedRouter history={history}>
          <Routes prefix={process.env.APP_CONTEXT_ROOT} />
        </ConnectedRouter>
      </MaterialUiThemeProvider>
    </Provider>
  );
}

export default process.env.NODE_ENV === 'development' ? hot(App) : App;
