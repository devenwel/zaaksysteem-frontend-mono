import { addDecorator, configure } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withA11y } from '@storybook/addon-a11y';

addDecorator(withKnobs);
addDecorator(withA11y);

const reqPackage = require.context('../packages', true, /.story.js$/);
const reqApps = require.context('../apps', true, /.story.js$/);
function loadStories() {
  reqPackage.keys().forEach(filename => reqPackage(filename));
  reqApps.keys().forEach(filename => reqApps(filename));
}

configure(loadStories, module);
